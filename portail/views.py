from django.views.generic import ListView
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.contrib.gis.geos import Point

from .models import Post


class PostListView(ListView):
    model = Post
    template_name = 'portail.html'


def DeletePostView(request):
    if request.method == 'POST':
        tel = request.POST['tel']
        try:
            post = Post.objects.get(tel__contains=tel)
            post.delete()
        except:
            return HttpResponse(status=409, reason='Numéro de téléphone inconnu !')
    return HttpResponse('ok')


def AddPostView(request):
    if request.method == 'POST':
        tel = request.POST['tel']
        coord_x = float(request.POST['coord_x'])
        coord_y = float(request.POST['coord_y'])
        coord = Point(coord_x, coord_y, srid=4326)
        if not Post.objects.filter(tel__contains=tel).exists():
            Post.objects.create(tel=tel, coord=coord)
        else:
            return HttpResponse(status=409, reason='Numéro de téléphone déjà enregistré !')
    return HttpResponse('ok')
